import java.awt.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.swing.JPanel

class clockPanel : JPanel() {


    init {
    }

    var lastPaint = System.currentTimeMillis()


    override fun getPreferredSize(): Dimension {
        return Dimension(200, 200)
    }

    fun getCurrentTime() : String {
        var currentDateTime = LocalDateTime.now();
        val formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy hh:mm:ss")
        val formattedTime = currentDateTime.format(formatter)
        return formattedTime.toString()
    }

    override fun paintComponent(g: Graphics?) {
        var now = System.currentTimeMillis();
        if(now - lastPaint >= 1000) {
            if(g != null) {
                lastPaint = now;
                g.color = Color.DARK_GRAY
                g.fillRect(0,0, 200, 200)
                g.color = Color.ORANGE
                g.drawString(getCurrentTime(), 10, 10)
                g.dispose()
                repaint()
            } else {
                println("graphics null.")
            }
        }
        repaint()
    }
}