import java.awt.BorderLayout
import java.awt.Dimension
import javax.swing.JFrame
import javax.swing.JPanel

fun display() {



    var frame = JFrame("Test")
    var clockRenderPanel = clockPanel()


    fun configureFrameSettings() {
        frame.size = Dimension(200, 200)
        frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        frame.layout = BorderLayout()
        frame.add(clockRenderPanel, BorderLayout.CENTER)
        frame.isVisible = true
        frame.pack()
        frame.validate()
    }

    configureFrameSettings()

}